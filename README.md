# How to

To add our repo to your helm, just type:

    helm repo add iotops https://iotops.gitlab.io/charts

To add a new chart, add it like a git submodule

[![](sponsored-by-3xm-group.png)](http://www.3xmgroup.com/)
